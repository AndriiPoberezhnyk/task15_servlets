package com.epam.training;

import com.epam.training.domain.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/users/orders/*")
public class PizzaServlet extends HttpServlet {
    private static Logger logger1 = LogManager.getLogger(UserServlet.class);
    private static Map<Integer, Pizza> pizzaList = new HashMap<>();

    @Override
    public void init() throws ServletException {
        logger1.info("Servlet " + this.getServletName() + " has started");
        Pizza p1 = new Pizza("Pineapple pizza");
        Pizza p2 = new Pizza("Four seasons");
        Pizza p3 = new Pizza("Salami");
        Pizza p4 = new Pizza("Margarita ");
        Pizza p5 = new Pizza("Diabola");
        pizzaList.put(p1.getId(), p1);
        pizzaList.put(p2.getId(), p2);
        pizzaList.put(p3.getId(), p3);
        pizzaList.put(p4.getId(), p4);
        pizzaList.put(p5.getId(), p5);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<p>" + req.getRequestURL() + "</p>");
        out.println("<p> <a href='orders'>REFRESH</a> </p>");
        out.println("<p> Current user " + UserServlet.currentUser + "</p>");
        out.println("<h2>Pizza menu</h2>");
        String backToUsersURL = req.getRequestURL().toString().replace(
                "/orders", "");
        out.println("<a href=\"" + backToUsersURL + "\">Go to users</a>");

        out.println("<h2>Menu</h2>");
        for (Pizza pizza : pizzaList.values()) {
            out.println("<p>" + pizza + "</p>");
        }

        out.println("<form action='orders' method='POST'>\n"
                + "  Pizza id: <input type='text' name='a_pizza_id'>\n"
                + "  <button type='submit'>Add pizza to order</button>\n"
                + "</form>");

        out.println("<h2>Your order</h2>");
        for (Pizza pizza : UserServlet.usersOrders
                .get(UserServlet.currentUser.getId())) {
            out.println("<p>" + pizza + "</p>");
        }

        out.println("<form>\n"
                + "  <p><b>DELETE ELEMENT</b></p>\n"
                + "  <p> Pizza id: <input type='text' name='r_pizza_id'>\n"
                + "    <input type='button' onclick='remove(this.form" +
                ".r_pizza_id" +
                ".value)' name='ok' value='Delete from order'/>\n"
                + "  </p>\n"
                + "</form>");

        out.println("<script type='text/javascript'>\n"
                + "  function remove(id) { fetch('orders/' + id, {method: "
                + "  'DELETE'}); }\n"
                + "</script>");

        // Echo client's request information
        out.println("<p>Request URI: " + req.getRequestURI() + "</p>");
        out.println("<p>Method: " + req.getMethod() + "</p>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger1.info("----------doPost------------");
        String pizzaId = req.getParameter("a_pizza_id");
        Pizza pizza = pizzaList.get(Integer.parseInt(pizzaId));
        logger1.info("Add " + pizza);
        List<Pizza> userOrders =
                UserServlet.usersOrders.get(UserServlet.currentUser.getId());
        userOrders.add(pizza);
        UserServlet.usersOrders
                .put(UserServlet.currentUser.getId(), userOrders);
        doGet(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger1.info("----------doDelete------------");
        String pizzaId = req.getRequestURI();
        logger1.info("URI=" + pizzaId);
        pizzaId = pizzaId.replace("/pizza-delivery-servlets/users/orders/", "");
        logger1.info("id=" + pizzaId);
        Pizza pizza = pizzaList.get(Integer.parseInt(pizzaId));
        List<Pizza> userOrders = UserServlet.usersOrders
                .get(UserServlet.currentUser.getId());
        if (userOrders.contains(pizza)) {
            userOrders.remove(pizza);
            UserServlet.usersOrders.put(UserServlet.currentUser.getId(), userOrders);
        } else {
            resp.sendError(404, "This pizza isn't in order ");
        }
    }
}
