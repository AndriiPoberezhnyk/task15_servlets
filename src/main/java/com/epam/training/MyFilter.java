package com.epam.training;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;


@WebFilter("/users/orders/*")
public class MyFilter implements Filter {

    private static Logger logger1 = LogManager.getLogger(MyFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain filterChain) throws IOException, ServletException {
        logger1.info("----------FILTER------------");
        Optional<String> pizzaId = Optional.ofNullable(req.getParameter(
                "a_pizza_id"));
        logger1.info("FILTER:" + pizzaId.orElse("--"));
        if (pizzaId.filter(id -> id.equals("1")).isPresent()) {
            ((HttpServletResponse) resp).sendError(403, "This is the point of" +
                    " no return!!! Out of pineapples:)");
            return;
        }
        if (UserServlet.currentUser == null) {
            ((HttpServletResponse) resp).sendError(404,
                    "Missing user! Authorize first to order pizza");
            return;
        }
        filterChain.doFilter(req, resp);
    }

    @Override
    public void destroy() {
    }
}