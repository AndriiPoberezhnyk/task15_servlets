package com.epam.training;

import com.epam.training.domain.Pizza;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/orders/*")
public class OrderServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<h2>Orders</h2>");
        out.println("<a href=\"users\">Go to users</a>");
        for (Integer key : UserServlet.usersOrders.keySet()) {
            out.println("<p>" + UserServlet.users.get(key) + "</p>");
            out.println("<p> Order: </p>");
            if (UserServlet.usersOrders.get(key).size() != 0) {
                for (Pizza pizza : UserServlet.usersOrders.get(key)) {
                    out.println("<p>" + pizza + "</p>");
                }
            } else {
                out.println("<p> 0 orders from this user</p>");
            }
        }
    }
}
