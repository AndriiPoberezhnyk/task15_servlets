package com.epam.training.domain;

public class Pizza {
    private static int unique = 1;
    private int id;
    private String name;

    public Pizza(String name) {
        this.name = name;
        id = unique;
        unique++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
