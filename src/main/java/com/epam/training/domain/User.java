package com.epam.training.domain;

public class User {
    private static int unique = 1;
    private int id;
    private String name;
    private String password;

    public User(String name) {
        this.name = name;
        id = unique;
        unique++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
