package com.epam.training;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import com.epam.training.domain.*;

@WebServlet("/users/*")
public class UserServlet extends HttpServlet {
    private static Logger logger1 = LogManager.getLogger(UserServlet.class);
    public static Map<Integer, User> users = new HashMap<>();
    public static User currentUser;
    public static Map<Integer, List<Pizza>> usersOrders;

    @Override
    public void init() throws ServletException {
        currentUser = null;
        usersOrders = new HashMap<>();
        logger1.info("Servlet " + this.getServletName() + " has started");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger1.info(this.getServletName() + " into doGet()");
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<p> <a href='users'>REFRESH</a> </p>");
        out.println("<p> <a href='users/orders'>Go to menu</a> </p>");
        out.println("<p> <a href='orders'>Go to orders</a> </p>");

        out.println("<h2>Users List</h2>");
        for (User user : users.values()) {
            out.println("<p>" + user + "</p>");
        }

        out.println("<form action='users' method='POST'>\n"
                + "  User Name: <input type='text' name='user_name'>\n"
                + "  <button type='submit'>Add or change user</button>\n"
                + "</form>");

        out.println("<form>\n"
                + "  <p><b>DELETE ELEMENT</b></p>\n"
                + "  <p> User id: <input type='text' name='user_id'>\n"
                + "    <input type='button' onclick='remove(this.form.user_id.value)' name='ok' value='Delete User'/>\n"
                + "  </p>\n"
                + "</form>");

        out.println("<script type='text/javascript'>\n"
                + "  function remove(id) { fetch('users/' + id, {method: "
                + " 'DELETE'}); }\n"
                + "</script>");

        // Echo client's request information
        out.println("<p>Request URI: " + req.getRequestURI() + "</p>");
        out.println("<p>Method: " + req.getMethod() + "</p>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger1.info("----------doPost------------");
        String newUserName = req.getParameter("user_name");
        if (isInteger(newUserName)) {
            Integer id = Integer.parseInt(newUserName);
            if (users.containsKey(id))
                currentUser = users.get(id);
        } else {
            logger1.info("Add " + newUserName);
            User newUser = new User(newUserName);
            users.put(newUser.getId(), newUser);
            usersOrders.put(newUser.getId(), new ArrayList<>());
            currentUser = newUser;
        }
        doGet(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger1.info("----------doDelete------------");
        String id = req.getRequestURI();
        logger1.info("URI=" + id);
        id = id.replace("/pizza-delivery-servlets/users/", "");
        logger1.info("id=" + id);
        users.remove(Integer.parseInt(id));
    }

    @Override
    public void destroy() {
        logger1.info("Servlet " + this.getServletName() + " has stopped");
    }

    private boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
